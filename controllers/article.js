const Article = require('../models/article');

/**
 * Vue pour créer un article
 * @param {*Request} req Requête envoyée par le client
 * @param {*Response} res Réponse envoyée au client
 * @param {*Callback} next Prochain callback dans la chaine de middleware
 */
exports.createArticle = (req, res, next) =>{

};

/**
 * Sauvegarde un article
 */
exports.storeArticle = (req, res, next) =>{

};

/**
 * Montre un article
 */
exports.getArticle = (req, res, next) => {

};

/**
 * Montre des article
 */
exports.getArticles = (req, res, next) => {
  Article.find({}, (err, _articles) => {
    return res.render('index', {
      title: 'Démonstration NodeJS',
      articles: _articles
    });
  });
};

/**
 * Vue pour modifier un article
 */
exports.editArticle = (req, res, next) => {

};

/**
 * Modifie un article
 */
exports.updateArticle = (req, res, next) => {
  Article.findOne({'_id': req.params.id}, (err, _article) => {
    if (err) return next(err);
    _article.titre = req.body.titre;
    _article.contenu = req.body.contenu;

    _article.save((err) => {
      if (err) return next(err);
    });

    res.redirect('/');
  });
};

/**
 * Supprimes un article
 */
exports.deleteArticle = (req, res, next) => {

};
