const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const articleSchema = new Schema({
    titre: String,
    contenu: String
});

const Article = mongoose.model('Article', articleSchema);
module.exports = Article;
