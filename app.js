const express = require('express');
const path = require('path');
const session = require('express-session');
const favicon = require('serve-favicon');
const MongoStore = require('connect-mongo')(session);
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const sassMiddleware = require('node-sass-middleware');

const app = express();

const articleController = require('./controllers/article');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/dev');
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

// Configuration du 'template engine'
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Configuration du 'transpiler' de SCSS
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass / false = .scss
  sourceMap: true
}));

app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: 'Your Session Secret goes here',
  store: new MongoStore({
    url: 'mongodb://localhost:27017/dev',
    autoReconnect: true,
    clear_interval: 3600
  })
}));

// Spécifier le répertoire publique de l'application pour restreindre l'accès
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', articleController.getArticles);
app.get('/article/create', articleController.createArticle);
app.post('/article/store', articleController.storeArticle);
app.get('/article/:id', articleController.getArticle);
app.get('/article/:id/edit', articleController.editArticle);
app.post('/article/:id/update', articleController.updateArticle);
app.post('/article/:id/delete', articleController.deleteArticle);

// Intercepter les erreurs 404
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // Montrer les détails de l'erreur en développement seulement
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
